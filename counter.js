var interval;

$(window).ready(interval = setInterval(handleTime, 1));

function handleTime() {
	var run = true;
	var current = new Date().getTime();
	var exams = new Date(2018, 4, 4, 9, 0, 0, 0).getTime();
	var seconds = (exams - current) / 1000;
	if(seconds > 0) {
		var days = Math.floor(seconds / (86400));
		seconds = seconds - (days * 86400);
		var hours = Math.floor(seconds / 3600);
		seconds = seconds - (hours * 3600);
		var minutes = Math.floor(seconds / 60);
		seconds = Math.floor(seconds - (minutes * 60));
		handleDays(days);
		handleHours(hours);
		handleMinutes(minutes);
		handleSeconds(seconds);
	} else {
		$("#status-title").html("Powodzenia (za rok hehe)");
		$("#timeleft-wrapper").css("display", "none");
	}
}

function handleDays(days) {
	$('#days-counter').html(days);
	var label;
	if(days == 1) label = "dzień";
	else label = "dni";
	$('#days-label').html(label);
}

function handleHours(hours) {
	$('#hours-counter').html(hours);
	var label = "godzin";
	if(hours == 1) label = "godzina";
	else if((hours % 10 >= 2 && hours % 10 <= 4) && xor(hours < 10, hours > 20)) label = "godziny";
	$('#hours-label').html(label);
}

function handleMinutes(minutes) {
	$('#minutes-counter').html(minutes);
	var label = "minut";
	if(minutes == 1) label = "minuta";
	else if((minutes % 10 >= 2 && minutes % 10 <= 4) && xor(minutes < 10, minutes > 20)) label = "minuty";
	$('#minutes-label').html(label);
}

function handleSeconds(seconds) {
	$('#seconds-counter').html(seconds);
	var label = "sekund";
	if(seconds == 1) label = "sekunda";
	else if((seconds % 10 >= 2 && seconds % 10 <= 4) && xor(seconds < 10, seconds > 20)) label = "sekundy";
	$('#seconds-label').html(label);
}

function xor(a, b) {
	return (a || b) && !(a && b);
}